import telebot
import numpy as np
import wave
import struct
import subprocess
from pydub import AudioSegment
import os
import time
import threading
import psycopg2
import random
from text import string
import math
from pysndfx import AudioEffectsChain
from scipy.io import wavfile

TOKEN = '1450283578:AAG0X3M40heNHPFCJUSAmfbLaS20IxgwXrQ'
apiKey = 'e56166cc7f09c95a5326626e8bc3e874'
SUPPORT_CHAT_ID = -1001498390248
ADMIN_CHAT_ID = 1093110311
TEST_PAYMENT_TOKEN = "381764678:TEST:20780"
PAYMENT_TOKEN = "390540012:LIVE:13496"
to_send_one_post_user_id = 0

bot = telebot.TeleBot(token=TOKEN, parse_mode='MARKDOWN')
conn = psycopg2.connect(dbname='voicebot',
                        user='voicebot',
                        password='2001',
                        host='46.17.104.151')
conn.autocommit = True
cursor = conn.cursor()

action_status = {}


def send_action(chat_id, action):
    global action_status
    while action_status[f"{chat_id}"]:
        bot.send_chat_action(chat_id=chat_id, action=action)
        time.sleep(4.5)


def sendMessage(chat_id, text, reply_markup=None, reply_to_message_id=None):
    try:
        bot.send_chat_action(chat_id=chat_id, action='typing')
        msg = bot.send_message(chat_id=chat_id,
                               text=text,
                               reply_markup=reply_markup,
                               reply_to_message_id=None)
    except telebot.apihelper.ApiTelegramException as error:
        msg = error.args
        print(msg)


def bot_start():
    print("listen bot...\n")
    bot.polling(none_stop=True, interval=0, timeout=20)


def thread_start():
    print('thread start...')
    while True:
        if time.strftime("%H", time.localtime()) == "00" and time.strftime("%M", time.localtime()) == "00":
            every_day()
            time.sleep(3600)
        else:
            time.sleep(10)


def every_day():
    # Users
    bot.send_message(chat_id=SUPPORT_CHAT_ID,
                     text="Ежедневная проверка оплаты pro-аккаунтов.")
    cursor.execute(f'SELECT * FROM users')
    users = cursor.fetchall()
    for user in users:
        if not user[4]:
            cursor.execute(f'UPDATE users SET edit_timbre = %s, edit_speed = %s '
                           f'WHERE chat_id = {user[0]}',
                           (10, 10))
        elif user[5] < time.time():
            cursor.execute(f'UPDATE users SET edit_timbre = %s, edit_speed = %s, expiry_date_pro = %s,'
                           f'pro_account = %s WHERE chat_id = {user[0]}',
                           (10, 10, 0, False))
            sendMessage(chat_id=user[0],
                        text=string[user[11]]["message"]["pro-account"]["cancel"],
                        reply_markup=None)

        else:
            cursor.execute(f'UPDATE users SET edit_timbre = %s, edit_speed = %s '
                           f'WHERE chat_id = {user[0]}',
                           (10, 10))
    # Promotions
    cursor.execute(f'SELECT * FROM promotion')
    promotions = cursor.fetchall()
    for promo in promotions:
        if promo[5]:
            if promo[4] < time.time():
                cursor.execute(f'UPDATE promotion SET status = {False} WHERE id = {promo[0]}')
                cursor.execute(f'SELECT * FROM users WHERE chat_id = {promo[6]}')
                data = cursor.fetchone()
                sendMessage(chat_id=promo[6],
                            text=f'{string[data[11]]["message"]["promotion"]["cancel"]} `{promo[1]}`')


def get_promo_code(num_chars):
    code_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    code_true = False
    while not code_true:
        code = ''
        for i in range(0, num_chars):
            slice_start = random.randint(0, len(code_chars) - 1)
            code += code_chars[slice_start: slice_start + 1]
        cursor.execute(f"SELECT * FROM promotion WHERE name_promotion = '{code}'")
        if cursor.fetchone() == None:
            code_true = True
    return code


def language(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    markup = telebot.types.InlineKeyboardMarkup()
    ru = telebot.types.InlineKeyboardButton(text='🇷🇺Русский', callback_data='ru')
    en = telebot.types.InlineKeyboardButton(text='🇺🇸English', callback_data='en')
    back = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["back"], callback_data='back_lang')
    markup.add(ru, en)
    if data[8] == "language":
        markup.add(back)
    sendMessage(chat_id=message.chat.id,
                text=string["lang-select"],
                reply_markup=markup)


def button_menu(lang):
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[lang]["button"]["balance"], string[lang]["button"]["setting"])
    keyboard.row(string[lang]["button"]["support"], string[lang]["button"]["help"])
    return keyboard


def keyboard_voice(data):
    keyboard = telebot.types.InlineKeyboardMarkup()
    timbre_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["timbre"],
                                                       callback_data='timbre')
    speed_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["speed"],
                                                      callback_data='speed')
    background_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["background"],
                                                           callback_data='background')
    effects_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["effects"],
                                                        callback_data='effects')
    cancel_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["cancel"],
                                                       callback_data='cancel')
    keyboard.add(timbre_button, speed_button)
    keyboard.add(background_button, effects_button)
    keyboard.add(cancel_button)
    return keyboard


def buy_pro(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("buy-pro",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["buy-month"], string[data[11]]["button"]["buy-year"])
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[11]]["message"]["pro-account"]["buy-pro"],
                reply_markup=keyboard)


def extend_pro(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("extend-pro",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["buy-month"], string[data[11]]["button"]["buy-year"])
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[11]]["message"]["pro-account"]["extend-pro"],
                reply_markup=keyboard)


def promotion(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("promotion",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["buy-promotion-month"], string[data[11]]["button"]["buy-promotion-year"])
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text=string[data[11]]["message"]["promotion"]["text"],
                reply_markup=keyboard)


def admin_panel(message):
    if message.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("panel-admin",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["all-send-post"], string[data[11]]["button"]["no-pro-send-post"])
    keyboard.row(string[data[11]]["button"]["one-user-send-post"], string[data[11]]["button"]["statistic"])
    keyboard.row(string[data[11]]["button"]["new-promotion"])
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text=string[data[11]]["message"]["admin-panel"]["text"],
                reply_markup=keyboard)


def all_send_post(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("all-send-post",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text=string[data[11]]["message"]["admin-panel"]["all-send-post"],
                reply_markup=keyboard)


def no_pro_send_post(message):
    if message.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("no-pro-send-post",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=ADMIN_CHAT_ID,
                text=string[data[11]]["message"]["admin-panel"]["no-pro-send-post"],
                reply_markup=keyboard)


def one_user_send_post(message):
    if message.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("one-user-send-post",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text={string["russian"]["message"]["admin-panel"]["one-user-send-post"]},
                reply_markup=keyboard)


def one_user_send_post_text(message):
    if message.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("one-user-send-post-text",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {to_send_one_post_user_id}')
    data = cursor.fetchone()
    print(data)
    if not data:
        one_user_send_post(message)
    else:
        sendMessage(chat_id=ADMIN_CHAT_ID,
                    text=f'{string["russian"]["message"]["admin-panel"]["one-user-send-post-text"]}'
                         f'{data[1]} {data[2]} | @{data[3]} | `{data[0]}`')


def statistic(message):
    if message.chat.id != ADMIN_CHAT_ID:
        return
    bot.send_chat_action(chat_id=message.chat.id, action='upload_document')
    count_pro = 0
    count_user = 0
    cursor.execute(f'SELECT * FROM users')
    users = cursor.fetchall()
    with open('files/users.txt', 'w') as f:
        for user in users:
            f.write(f"{user[0]} | {user[1]} {user[2]} | {user[3]} | {user[11]}"
                    f" | pro: {user[4]}\n".encode('utf-8').decode('utf-8'))
            if user[4]:
                count_pro += 1
            count_user += 1
    cursor.execute(f'SELECT * FROM promotion')
    promotions = cursor.fetchall()
    with open('files/promotions.txt', 'w') as f:
        for promotion in promotions:
            f.write(f"{promotion[1]}|{promotion[3]}|{promotion[5]}|{promotion[6]}|{promotion[7]}\n")
    try:
        bot.send_media_group(chat_id=ADMIN_CHAT_ID,
                             media=[
                                 telebot.types.InputMediaDocument(media=open('files/users.txt', 'rb'),
                                                                  caption='Список пользователей☝\n'
                                                                          f'Всего пользователей: {count_user}\n'
                                                                          f'Пользователей с pro-аккаунтоами: {count_pro}\n'),
                                 telebot.types.InputMediaDocument(media=open('files/promotions.txt', 'rb'),
                                                                  caption='Список промокодов☝\n'),
                             ])
    except telebot.apihelper.ApiTelegramException as error:
        msg = error.args
        print(msg)
    admin_panel(message)


def new_promotion(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    if message.chat.id != ADMIN_CHAT_ID:
        return
    cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("new-promotion",))
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
    keyboard.row(string[data[11]]["button"]["back"])
    sendMessage(chat_id=message.chat.id,
                text={string["russian"]["message"]["admin-panel"]["new-promotion"]},
                reply_markup=keyboard)


@bot.message_handler(commands=['start'])
def start(message):
    if message.chat.type != 'private':
        return
    else:
        bot.send_chat_action(chat_id=message.chat.id, action='typing')
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        if not data:
            cursor.execute(f'INSERT INTO users (chat_id, first_name, last_name, user_name, pro_account, '
                           f'expiry_date_pro, edit_timbre, edit_speed, status, count_timbre, count_speed, lang) '
                           f'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                           (message.chat.id, str(message.from_user.first_name), str(message.from_user.last_name),
                            str(message.from_user.username), False, 0, 10, 10, "start", 0, 0, "russian"))
            language(message)
        else:
            cursor.execute(f'UPDATE users SET first_name = %s, last_name = %s, user_name = %s,'
                           f'status = %s WHERE chat_id = {message.chat.id}',
                           (str(message.from_user.first_name), str(message.from_user.last_name),
                            str(message.from_user.username), "start"))
            cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
            data = cursor.fetchone()
            keyboard = button_menu(data[11])
            sendMessage(chat_id=message.chat.id,
                        text=string[data[11]]["message"]["restart"],
                        reply_markup=keyboard)


@bot.message_handler(commands=['help'])
def help(message):
    if message.chat.type != 'private':
        return
    else:
        bot.send_chat_action(chat_id=message.chat.id, action='typing')
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("help",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[11]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[11]]["message"]["help"],
                    reply_markup=keyboard)


@bot.message_handler(commands=['setting'])
def setting(message):
    try:
        if message.chat.type != 'private':
            return
        else:
            bot.send_chat_action(chat_id=message.chat.id, action='typing')
            cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("setting",))
            cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
            data = cursor.fetchone()
            print(data)
            keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
            keyboard.row(string[data[11]]["button"]["language"])
            if message.chat.id == ADMIN_CHAT_ID:
                keyboard.row(string[data[11]]["button"]["admin-panel"])
            keyboard.row(string[data[11]]["button"]["back"])
            sendMessage(chat_id=message.chat.id,
                        text=string[data[11]]["message"]["setting"],
                        reply_markup=keyboard)
    except Exception as error:
        print(error.args)


@bot.message_handler(commands=['balance'])
def balance(message):
    if message.chat.type != 'private':
        return
    else:
        bot.send_chat_action(chat_id=message.chat.id, action='typing')
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("balance",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        if data[4] == False:
            keyboard.row(string[data[11]]["button"]["buy-pro"], string[data[11]]["button"]["promotion"])
            keyboard.row(string[data[11]]["button"]["update"])
            text_balance = ''
        else:
            keyboard.row(string[data[11]]["button"]["extend-pro"], string[data[11]]["button"]["promotion"])
            keyboard.row(string[data[11]]["button"]["update"])
            text_balance = f"{math.ceil((data[5] - time.time()) / 86400)}"
        keyboard.row(string[data[11]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[11]]["message"]["pro-account"][data[4]] + text_balance,
                    reply_markup=keyboard)


@bot.message_handler(commands=['support'])
def support(message):
    if message.chat.type != 'private':
        return
    else:
        bot.send_chat_action(chat_id=message.chat.id, action='typing')
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("support",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
        keyboard.row(string[data[11]]["button"]["back"])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[11]]["message"]["support"],
                    reply_markup=keyboard)


@bot.message_handler(commands=['cancel'])
def cancel(message):
    if message.chat.type != 'private':
        return
    else:
        bot.send_chat_action(chat_id=message.chat.id, action='typing')
        cursor.execute(f'UPDATE users SET status = %s WHERE chat_id = {message.chat.id}', ("start",))
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        keyboard = button_menu(data[11])
        sendMessage(chat_id=message.chat.id,
                    text=string[data[11]]["message"]["start"],
                    reply_markup=keyboard)


@bot.message_handler(content_types=['voice'])
def handler_voice(message):
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    if not data[4]:
        if message.voice.duration > 10:
            sendMessage(chat_id=message.chat.id,
                        text=string[data[11]]["message"]["cancel-result"])
            return
    global action_status
    action_status[f"{message.chat.id}"] = True
    send_act = threading.Thread(target=send_action,
                                args=(message.chat.id, 'record_voice',),
                                name=f"{message.chat.id}")
    send_act.start()
    file_info = bot.get_file(message.voice.file_id)
    audio = bot.download_file(file_info.file_path)
    with open(f'files/{message.chat.id}.ogg', 'wb') as f:
        f.write(audio)
    keyboard = keyboard_voice(data)
    action_status[f"{message.chat.id}"] = False
    bot.send_voice(chat_id=message.chat.id,
                   voice=open(f'files/{message.chat.id}.ogg', 'rb'),
                   caption=string[data[11]]["message"]["advertising"],
                   reply_markup=keyboard)


@bot.message_handler(content_types=['text'])
def message(message):
    global to_send_one_post_user_id
    if message.chat.id == SUPPORT_CHAT_ID:  # If messages come from the support service, we send them to the user
        cursor.execute(f"SELECT * FROM messages WHERE message_id = {message.reply_to_message.message_id};")
        data = cursor.fetchone()
        try:
            msg = bot.send_message(
                chat_id=data[0],
                reply_to_message_id=data[2],
                text=f"{message.text}")
            cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                           f"VALUES (0, {msg.message_id}, {message.message_id});")
        except telebot.apihelper.ApiTelegramException as error:
            msg = error.args[0]
    else:
        cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
        data = cursor.fetchone()
        print(data)
        if message.reply_to_message is not None:
            if message.reply_to_message.content_type == 'text':
                cursor.execute(f'SELECT * FROM messages WHERE from_id = 0 AND '
                               f'message_id = {message.reply_to_message.message_id};')
                data = cursor.fetchone()
                if data is not None:
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}",
                                               reply_to_message_id=data[2])
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
                else:
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"`{message.reply_to_message.text}`\n\n"
                                                    f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}")
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
        if data[8] == 'start':
            if message.text == string[data[11]]["button"]["balance"]:
                balance(message)
            elif message.text == string[data[11]]["button"]["setting"]:
                setting(message)
            elif message.text == string[data[11]]["button"]["support"]:
                support(message)
            elif message.text == string[data[11]]["button"]["help"]:
                help(message)
        elif data[8] == 'balance':
            if message.text == string[data[11]]["button"]["back"]:
                cancel(message)
            elif message.text == string[data[11]]["button"]["buy-pro"]:
                buy_pro(message)
            elif message.text == string[data[11]]["button"]["update"]:
                balance(message)
            elif message.text == string[data[11]]["button"]["extend-pro"]:
                extend_pro(message)
            elif message.text == string[data[11]]["button"]["promotion"]:
                promotion(message)
        elif data[8] == 'setting':
            if message.text == string[data[11]]["button"]["language"]:
                cursor.execute(f"UPDATE users SET status = 'language' WHERE chat_id = {message.chat.id}")
                language(message)
            elif message.text == string[data[11]]["button"]["admin-panel"]:
                admin_panel(message)
            elif message.text == string[data[11]]["button"]["back"]:
                cancel(message)
        elif data[8] == 'support':  # User support service
            if message.text == string[data[11]]["button"]["back"]:
                cancel(message)
            else:
                print(message)
                if message.reply_to_message is None:
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}", )
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
                else:
                    cursor.execute(f'SELECT * FROM messages WHERE from_id = 0 AND '
                                   f'message_id = {message.reply_to_message.message_id};')
                    data = cursor.fetchone()
                    try:
                        msg = bot.send_message(chat_id=SUPPORT_CHAT_ID,
                                               text=f"{message.from_user.first_name} {message.from_user.last_name}"
                                                    f" | @{message.from_user.username}\n"
                                                    f"chat-id = {message.chat.id}\n{message.text}",
                                               reply_to_message_id=data[2])
                        cursor.execute("INSERT INTO messages (from_id, message_id, message_from_id) "
                                       f"VALUES (%s, %s, %s)", (message.chat.id, msg.message_id, message.message_id,))
                    except telebot.apihelper.ApiTelegramException as error:
                        msg = error.args[0]
                        print(msg)
        elif data[8] == 'help':
            if message.text == string[data[11]]["button"]["back"]:
                cancel(message)
        elif data[8] == 'language':
            bot.delete_message(chat_id=message.chat.id,
                               message_id=message.message_id)
        elif data[8] == 'buy-pro':
            bot.send_chat_action(chat_id=message.chat.id, action='typing')
            if message.text == string[data[11]]["button"]["back"]:
                balance(message)
            elif message.text == string[data[11]]["button"]["buy-month"]:
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[11]]["message"]["buy-month"]["title"],
                                 description=string[data[11]]["message"]["buy-month"]["description"],
                                 invoice_payload='buy-one-month',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-month',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[11]]["message"]["buy-month"]["price"],
                                     amount=10000)],
                                 is_flexible=False)
            elif message.text == string[data[11]]["button"]["buy-year"]:
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[11]]["message"]["buy-year"]["title"],
                                 description=string[data[11]]["message"]["buy-year"]["description"],
                                 invoice_payload='buy-one-year',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-year',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[11]]["message"]["buy-year"]["price"],
                                     amount=100000)],
                                 is_flexible=False)
        elif data[8] == 'extend-pro':
            if message.text == string[data[11]]["button"]["back"]:
                balance(message)
            elif message.text == string[data[11]]["button"]["buy-month"]:
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[11]]["message"]["buy-month"]["title"],
                                 description=string[data[11]]["message"]["buy-month"]["description"],
                                 invoice_payload='extend-one-month',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-month',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[11]]["message"]["buy-month"]["price"],
                                     amount=10000)],
                                 is_flexible=False)
            elif message.text == string[data[11]]["button"]["buy-year"]:
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[11]]["message"]["buy-year"]["title"],
                                 description=string[data[11]]["message"]["buy-year"]["description"],
                                 invoice_payload='extend-one-year',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-year',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[11]]["message"]["buy-year"]["price"],
                                     amount=100000)],
                                 is_flexible=False)
        elif data[8] == 'promotion':
            if message.text == string[data[11]]["button"]["back"]:
                balance(message)
            elif message.text == string[data[11]]["button"]["buy-promotion-month"]:
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[11]]["message"]["buy-month"]["title"],
                                 description=string[data[11]]["message"]["buy-month"]["description"],
                                 invoice_payload='buy-promotion-one-month',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-month',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[11]]["message"]["buy-month"]["price"],
                                     amount=10000)],
                                 is_flexible=False)
            elif message.text == string[data[11]]["button"]["buy-promotion-year"]:
                bot.send_invoice(chat_id=message.chat.id,
                                 title=string[data[11]]["message"]["buy-year"]["title"],
                                 description=string[data[11]]["message"]["buy-year"]["description"],
                                 invoice_payload='buy-promotion-one-year',
                                 provider_token=PAYMENT_TOKEN,
                                 start_parameter='payload-one-year',
                                 currency='RUB',
                                 prices=[telebot.types.LabeledPrice(
                                     label=string[data[11]]["message"]["buy-year"]["price"],
                                     amount=100000)],
                                 is_flexible=False)
            else:
                cursor.execute(f"SELECT * FROM promotion WHERE name_promotion = '{message.text}'")
                code = cursor.fetchone()
                if not code:
                    sendMessage(chat_id=message.chat.id,
                                text=string[data[11]]["message"]["promotion"]["none-code"])
                    balance(message)
                elif code[5] == False or code[4] < time.time():
                    sendMessage(chat_id=message.chat.id,
                                text=string[data[11]]["message"]["promotion"][False])
                    balance(message)
                else:
                    if data[5] == 0:
                        expiry_date_pro = time.time() + code[3] * 86400
                    else:
                        expiry_date_pro = data[5] + code[3] * 86400
                    cursor.execute(f'UPDATE users SET expiry_date_pro = %s, pro_account = %s '
                                   f'WHERE chat_id = {message.chat.id}',
                                   (expiry_date_pro, True))
                    cursor.execute(f'UPDATE promotion SET status = %s, used_user_id = %s WHERE id = {code[0]}',
                                   (False, data[0]))
                    sendMessage(chat_id=message.chat.id,
                                text=string[data[11]]["message"]["promotion"][True])
                    sendMessage(chat_id=code[6],
                                text=f'{data[1]} {data[2]} {string[data[11]]["message"]["promotion"]["activated"]} '
                                     f'`{code[1]}`')
                    balance(message)
        elif data[8] == 'panel-admin':
            if message.text == string[data[11]]["button"]["back"]:
                setting(message)
            elif message.text == "Опубликовать пост всем":
                all_send_post(message)
            elif message.text == "Опубликовать пост не pro":
                no_pro_send_post(message)
            elif message.text == "Отправить пост одному":
                one_user_send_post(message)
            elif message.text == "Получить статистику":
                statistic(message)
            elif message.text == "Создать промокод":
                new_promotion(message)
        elif data[8] == 'all-send-post':
            if message.text == string[data[11]]["button"]["back"]:
                admin_panel(message)
            else:
                cursor.execute(f'SELECT * FROM users')
                users = cursor.fetchall()
                for user in users:
                    sendMessage(chat_id=user[0],
                                text=f'*От администраторов:*\n{message.text}')
                admin_panel(message)
        elif data[8] == 'no-pro-send-post':
            if message.text == string[data[11]]["button"]["back"]:
                admin_panel(message)
            else:
                cursor.execute(f'SELECT * FROM users WHERE pro_account = {False}')
                users = cursor.fetchall()
                for user in users:
                    sendMessage(chat_id=user[0],
                                text=f'*От администраторов:*\n{message.text}')
                admin_panel(message)
        elif data[8] == 'one-user-send-post':
            if message.text == string[data[11]]["button"]["back"]:
                admin_panel(message)
            else:
                try:
                    to_send_one_post_user_id = int(message.text)
                except ValueError as error:
                    pass
                if to_send_one_post_user_id > 0:
                    one_user_send_post_text(message)
                else:
                    one_user_send_post(message)
        elif data[8] == 'one-user-send-post-text':
            if message.text == string[data[11]]["button"]["back"]:
                one_user_send_post(message)
            else:
                sendMessage(chat_id=to_send_one_post_user_id,
                            text=f'*От администраторов:*\n{message.text}')
                to_send_one_post_user_id = 0
                admin_panel(message)
        elif data[8] == 'new-promotion':
            if message.text == string[data[11]]["button"]["back"]:
                admin_panel(message)
            else:
                try:
                    day = int(message.text)
                except ValueError as error:
                    new_promotion(message)
                if day != None:
                    code = get_promo_code(12)
                    date_of_action = time.time() + 2592000
                    cursor.execute(
                        f'INSERT INTO promotion (name_promotion, pro_account, count_days, date_of_action, status, '
                        f'create_user_id) VALUES (%s, %s, %s, %s, %s, %s)',
                        (code, True, day, date_of_action, True, message.from_user.id))
                    sendMessage(chat_id=message.chat.id,
                                text=f'{string[data[11]]["message"]["admin-panel"]["new-promotion-true"]} `{code}`',
                                reply_to_message_id=message.message_id)
                    admin_panel(message)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    global action_status
    cursor.execute(f"SELECT * FROM users WHERE chat_id = {call.message.chat.id}")
    data = cursor.fetchone()
    if call.data == 'ru':
        if data[8] == "start":
            bot.answer_callback_query(callback_query_id=call.id, text='Установлен язык: Русский')
            cursor.execute(f"UPDATE users SET lang = 'russian' WHERE chat_id = {call.message.chat.id}")
            keyboard = button_menu("russian")
            try:
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
                msg = bot.send_message(chat_id=call.message.chat.id,
                                       text=f"Привет, *{call.message.chat.first_name}*, "
                                            f"этот бот создан для изменения голосовых сообщений.\n"
                                            f"Чтобы начать работать, пришли мне голосовое сообщение.\n"
                                            f"Для остального есть меню.👇🏽",
                                       reply_markup=keyboard)
            except telebot.apihelper.ApiTelegramException as error:
                msg = error.args[0]
        elif data[8] == "language":
            bot.answer_callback_query(callback_query_id=call.id, text='Установлен язык: Русский')
            bot.delete_message(chat_id=call.message.chat.id,
                               message_id=call.message.message_id)
            cursor.execute(f"UPDATE users SET lang = 'russian' WHERE chat_id = {call.message.chat.id}")
            setting(call.message)
    elif call.data == 'en':
        if data[8] == "start":
            bot.answer_callback_query(callback_query_id=call.id, text='Installed language: English')
            cursor.execute(f"UPDATE users SET lang = 'english' WHERE chat_id = {call.message.chat.id}")
            keyboard = button_menu("english")
            try:
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
                msg = bot.send_message(chat_id=call.message.chat.id,
                                       text=f"Hi, *{call.message.chat.first_name}*, "
                                            f"this bot was created to change voice messages\n"
                                            f"To get started, send me a voice message\n"
                                            f"For the rest there is a menu.👇🏽",
                                       reply_markup=keyboard)
            except telebot.apihelper.ApiTelegramException as error:
                msg = error.args[0]
        elif data[8] == "language":
            bot.answer_callback_query(callback_query_id=call.id, text='Установлен язык: Русский')
            bot.delete_message(chat_id=call.message.chat.id,
                               message_id=call.message.message_id)
            cursor.execute(f"UPDATE users SET lang = 'english' WHERE chat_id = {call.message.chat.id}")
            setting(call.message)
    elif call.data == 'back_lang':
        if data[8] == "language":
            try:
                bot.delete_message(chat_id=call.message.chat.id,
                                   message_id=call.message.message_id)
            except telebot.apihelper.ApiTelegramException as error:
                pass
            setting(call.message)
    elif call.data == 'timbre':
        bot.answer_callback_query(callback_query_id=call.id)
        keyboard = telebot.types.InlineKeyboardMarkup()
        timbre_up_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["timbre-up"],
                                                              callback_data='timbre-up')
        timbre_down_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["timbre-down"],
                                                                callback_data='timbre-down')
        back_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["back"],
                                                         callback_data='back')
        keyboard.add(timbre_down_button, timbre_up_button)
        keyboard.add(back_button)
        bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                      message_id=call.message.message_id,
                                      reply_markup=keyboard)
    elif call.data == 'speed':
        bot.answer_callback_query(callback_query_id=call.id)
        keyboard = telebot.types.InlineKeyboardMarkup()
        speed_faster_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["speed-faster"],
                                                                 callback_data='speed-faster')
        speed_slower_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["speed-slower"],
                                                                 callback_data='speed-slower')
        back_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["back"],
                                                         callback_data='back')
        keyboard.add(speed_slower_button, speed_faster_button)
        keyboard.add(back_button)
        bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                      message_id=call.message.message_id,
                                      reply_markup=keyboard)
    elif call.data == 'background':
        bot.answer_callback_query(callback_query_id=call.id)
        keyboard = telebot.types.InlineKeyboardMarkup()
        back_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["back"],
                                                         callback_data='back')
        keyboard.add(back_button)
        bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                      message_id=call.message.message_id,
                                      reply_markup=keyboard)
    elif call.data == 'effects':
        bot.answer_callback_query(callback_query_id=call.id)
        keyboard = telebot.types.InlineKeyboardMarkup()
        back_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["back"],
                                                         callback_data='back')
        reverb_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["reverb"],
                                                           callback_data='reverb')
        delay_button = telebot.types.InlineKeyboardButton(text=string[data[11]]["button"]["delay"],
                                                          callback_data='delay')
        keyboard.add(reverb_button, delay_button)
        keyboard.add(back_button)
        bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                      message_id=call.message.message_id,
                                      reply_markup=keyboard)
    elif call.data == 'back':
        bot.answer_callback_query(callback_query_id=call.id)
        keyboard = keyboard_voice(data)
        bot.edit_message_reply_markup(chat_id=call.message.chat.id,
                                      message_id=call.message.message_id,
                                      reply_markup=keyboard)
    elif call.data == 'timbre-up':
        if data[4] or int(data[6]) > 0:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"][call.data])
            action_status[f"{call.message.chat.id}"] = True
            send_act = threading.Thread(target=send_action,
                                        args=(call.message.chat.id, 'record_voice',),
                                        name=f"{call.message.chat.id}")
            send_act.start()
            save_voice(call.message)
            timbre_up(call.message)
            send_voice(call.message)
            keyboard = keyboard_voice(data)
            if call.message.caption == "Изменено в @VoiceEffects_bot":
                caption = ""
            else:
                caption = call.message.caption[0:-29]
            action_status[f"{call.message.chat.id}"] = False
            bot.send_voice(chat_id=call.message.chat.id,
                           voice=open(f'files/{call.message.chat.id}_.ogg', 'rb'),
                           caption=f'{caption}'
                                   f'{string[data[11]]["history"][call.data]}\n'
                                   f'\n{string[data[11]]["message"]["advertising"]}',
                           reply_markup=keyboard)
            os.remove(f'files/{call.message.chat.id}_.ogg')
            if data[4]:
                cursor.execute(f'UPDATE users SET count_timbre = {int(data[9]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
            else:
                cursor.execute(f'UPDATE users SET edit_timbre = {int(data[6]) - 1},'
                               f' count_timbre = {int(data[9]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
        else:
            bot.answer_callback_query(callback_query_id=call.id,
                                      text=string[data[11]]["message"]["answer"]["cannot_timbre"])
    elif call.data == 'timbre-down':
        if data[4] or int(data[6]) > 0:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"][call.data])
            action_status[f"{call.message.chat.id}"] = True
            send_act = threading.Thread(target=send_action,
                                        args=(call.message.chat.id, 'record_voice',),
                                        name=f"{call.message.chat.id}")
            send_act.start()
            save_voice(call.message)
            timbre_down(call.message)
            send_voice(call.message)
            keyboard = keyboard_voice(data)
            if call.message.caption == "Изменено в @VoiceEffects_bot":
                caption = ""
            else:
                caption = call.message.caption[0:-29]
            action_status[f"{call.message.chat.id}"] = False
            bot.send_voice(chat_id=call.message.chat.id,
                           voice=open(f'files/{call.message.chat.id}_.ogg', 'rb'),
                           caption=f'{caption}'
                                   f'{string[data[11]]["history"][call.data]}\n'
                                   f'\n{string[data[11]]["message"]["advertising"]}',
                           reply_markup=keyboard)
            os.remove(f'files/{call.message.chat.id}_.ogg')
            if data[4]:
                cursor.execute(f'UPDATE users SET count_timbre = {int(data[9]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
            else:
                cursor.execute(f'UPDATE users SET edit_timbre = {int(data[6]) - 1},'
                               f' count_timbre = {int(data[9]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
        else:
            bot.answer_callback_query(callback_query_id=call.id,
                                      text=string[data[11]]["message"]["answer"]["cannot_timbre"])
    elif call.data == 'speed-faster':
        if data[4] or int(data[7]) > 0:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"][call.data])
            action_status[f"{call.message.chat.id}"] = True
            send_act = threading.Thread(target=send_action,
                                        args=(call.message.chat.id, 'record_voice',),
                                        name=f"{call.message.chat.id}")
            send_act.start()
            save_voice(call.message)
            speed_faster(call.message)
            send_voice(call.message)
            keyboard = keyboard_voice(data)
            if call.message.caption == "Изменено в @VoiceEffects_bot":
                caption = ""
            else:
                caption = call.message.caption[0:-29]
            action_status[f"{call.message.chat.id}"] = False
            bot.send_voice(chat_id=call.message.chat.id,
                           voice=open(f'files/{call.message.chat.id}_.ogg', 'rb'),
                           caption=f'{caption}'
                                   f'{string[data[11]]["history"][call.data]}\n'
                                   f'\n{string[data[11]]["message"]["advertising"]}',
                           reply_markup=keyboard)
            os.remove(f'files/{call.message.chat.id}_.ogg')
            if data[4]:
                cursor.execute(f'UPDATE users SET count_speed = {int(data[10]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
            else:
                cursor.execute(f'UPDATE users SET edit_speed = {int(data[7]) - 1}, '
                               f'count_speed = {int(data[10]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
        else:
            bot.answer_callback_query(callback_query_id=call.id,
                                      text=string[data[11]]["message"]["answer"]["cannot_voice"])
    elif call.data == 'speed-slower':
        if int(data[7]) > 0 or data[4]:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"][call.data])
            action_status[f"{call.message.chat.id}"] = True
            send_act = threading.Thread(target=send_action,
                                        args=(call.message.chat.id, 'record_voice',),
                                        name=f"{call.message.chat.id}")
            send_act.start()
            save_voice(call.message)
            speed_slower(call.message)
            send_voice(call.message)
            keyboard = keyboard_voice(data)
            if call.message.caption == "Изменено в @VoiceEffects_bot":
                caption = ""
            else:
                caption = call.message.caption[0:-29]
            action_status[f"{call.message.chat.id}"] = False
            bot.send_voice(chat_id=call.message.chat.id,
                           voice=open(f'files/{call.message.chat.id}_.ogg', 'rb'),
                           caption=f'{caption}'
                                   f'{string[data[11]]["history"][call.data]}\n'
                                   f'\n{string[data[11]]["message"]["advertising"]}',
                           reply_markup=keyboard)
            os.remove(f'files/{call.message.chat.id}_.ogg')
            if data[4]:
                cursor.execute(f'UPDATE users SET count_speed = {int(data[10]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
            else:
                cursor.execute(f'UPDATE users SET edit_speed = {int(data[7]) - 1},'
                               f' count_speed = {int(data[10]) + 1} '
                               f'WHERE chat_id = {call.message.chat.id}')
        else:
            bot.answer_callback_query(callback_query_id=call.id,
                                      text=string[data[11]]["message"]["answer"]["cannot_voice"])
    elif call.data == 'reverb':
        if data[4]:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"][call.data])
            action_status[f"{call.message.chat.id}"] = True
            send_act = threading.Thread(target=send_action,
                                        args=(call.message.chat.id, 'record_voice',),
                                        name=f"{call.message.chat.id}")
            send_act.start()
            save_voice(call.message)
            fx = (AudioEffectsChain().reverb())
            fx(f'files/{call.message.chat.id}.wav', f'files/{call.message.chat.id}_.wav')
            send_voice(call.message)
            keyboard = keyboard_voice(data)
            if call.message.caption == "Изменено в @VoiceEffects_bot":
                caption = ""
            else:
                caption = call.message.caption[0:-29]
            action_status[f"{call.message.chat.id}"] = False
            bot.send_voice(chat_id=call.message.chat.id,
                           voice=open(f'files/{call.message.chat.id}_.ogg', 'rb'),
                           caption=f'{caption}'
                                   f'{string[data[11]]["history"][call.data]}\n'
                                   f'\n{string[data[11]]["message"]["advertising"]}',
                           reply_markup=keyboard)
            os.remove(f'files/{call.message.chat.id}_.ogg')
        else:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"]["cannot"])
    elif call.data == 'delay':
        if data[4]:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"][call.data])
            action_status[f"{call.message.chat.id}"] = True
            send_act = threading.Thread(target=send_action,
                                        args=(call.message.chat.id, 'record_voice',),
                                        name=f"{call.message.chat.id}")
            send_act.start()
            save_voice(call.message)
            fx = (AudioEffectsChain().delay(gain_out=0.8))
            fx(f'files/{call.message.chat.id}.wav', f'files/{call.message.chat.id}_.wav')
            send_voice(call.message)
            keyboard = keyboard_voice(data)
            if call.message.caption == "Изменено в @VoiceEffects_bot":
                caption = ""
            else:
                caption = call.message.caption[0:-29]
            action_status[f"{call.message.chat.id}"] = False
            bot.send_voice(chat_id=call.message.chat.id,
                           voice=open(f'files/{call.message.chat.id}_.ogg', 'rb'),
                           caption=f'{caption}'
                                   f'{string[data[11]]["history"][call.data]}\n'
                                   f'\n{string[data[11]]["message"]["advertising"]}',
                           reply_markup=keyboard)
            os.remove(f'files/{call.message.chat.id}_.ogg')
        else:
            bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"]["cannot"])
    elif call.data == 'cancel':
        bot.answer_callback_query(callback_query_id=call.id, text=string[data[11]]["message"]["answer"][call.data])
        bot.delete_message(chat_id=call.message.chat.id,
                           message_id=call.message.message_id)


@bot.pre_checkout_query_handler(func=lambda query: True)
def checkout(pre_checkout_query):
    print(pre_checkout_query)
    bot.answer_pre_checkout_query(pre_checkout_query.id, ok=True)


@bot.message_handler(content_types=['successful_payment'])
def got_payment(message):
    bot.send_chat_action(chat_id=message.chat.id, action='typing')
    cursor.execute(f'SELECT * FROM users WHERE chat_id = {message.chat.id}')
    data = cursor.fetchone()
    print(message.successful_payment.invoice_payload)
    if message.successful_payment.invoice_payload == 'buy-one-year':
        if data[5] == 0:
            expiry_date_pro = time.time() + 31536000
        else:
            expiry_date_pro = data[5] + 31536000
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, pro_account = %s '
                       f'WHERE chat_id = {message.chat.id}', (expiry_date_pro, True,))
        balance(message)
    elif message.successful_payment.invoice_payload == 'buy-one-month':
        if data[5] == 0:
            expiry_date_pro = time.time() + 2678400
        else:
            expiry_date_pro = data[5] + 2678400
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, pro_account = %s '
                       f'WHERE chat_id = {message.chat.id}', (expiry_date_pro, True,))
        balance(message)
    elif message.successful_payment.invoice_payload == 'extend-one-year':
        if data[5] == 0:
            expiry_date_pro = time.time() + 31536000
        else:
            expiry_date_pro = data[5] + 31536000
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, pro_account = %s '
                       f'WHERE chat_id = {message.chat.id}', (expiry_date_pro, True,))
        balance(message)
    elif message.successful_payment.invoice_payload == 'extend-one-month':
        if data[5] == 0:
            expiry_date_pro = time.time() + 2678400
        else:
            expiry_date_pro = data[5] + 2678400
        cursor.execute(f'UPDATE users SET expiry_date_pro = %s, pro_account = %s '
                       f'WHERE chat_id = {message.chat.id}', (expiry_date_pro, True,))
        balance(message)
    elif message.successful_payment.invoice_payload == 'buy-promotion-one-year':
        code = get_promo_code(12)
        date_of_action = time.time() + 2592000
        cursor.execute(f'INSERT INTO promotion (name_promotion, pro_account, count_days, date_of_action, status, '
                       f'create_user_id) VALUES (%s, %s, %s, %s, %s, %s)',
                       (code, True, 365, date_of_action, True, message.from_user.id))
        sendMessage(chat_id=message.chat.id,
                    text=f'{string[data[11]]["message"]["promotion"]["buy-year"]} `{code}`')
        promotion(message)
    elif message.successful_payment.invoice_payload == 'buy-promotion-one-month':
        code = get_promo_code(12)
        date_of_action = time.time() + 2592000
        cursor.execute(f'INSERT INTO promotion (name_promotion, pro_account, count_days, date_of_action, status, '
                       f'create_user_id) VALUES (%s, %s, %s, %s, %s, %s)',
                       (code, True, 30, date_of_action, True, message.from_user.id))
        sendMessage(chat_id=message.chat.id,
                    text=f'{string[data[11]]["message"]["promotion"]["buy-month"]} `{code}`')
        promotion(message)


def send_voice(message):
    sound = AudioSegment.from_wav(f'files/{message.chat.id}_.wav')
    sound.export(f'files/{message.chat.id}_.ogg', format="opus")
    os.remove(f'files/{message.chat.id}.wav')
    os.remove(f'files/{message.chat.id}_.wav')


def save_voice(message):
    file_info = bot.get_file(message.voice.file_id)
    audio = bot.download_file(file_info.file_path)
    with open(f'files/{message.chat.id}.ogg', 'wb') as f:
        f.write(audio)
    process = subprocess.run(['ffmpeg', '-i', f'files/{message.chat.id}.ogg', f'files/{message.chat.id}.wav'])
    if process.returncode != 0:
        print("no decode")
    os.remove(f'files/{message.chat.id}.ogg')


def speed_faster(message):
    fps, bowl_sound = wavfile.read(f'files/{message.chat.id}.wav')
    n = 2 ** (1.0 * 4 / 12.0)
    transposed = speed_voice(bowl_sound, n)
    wavfile.write(f'files/{message.chat.id}_.wav', 48000, transposed)


def speed_slower(message):
    fps, bowl_sound = wavfile.read(f'files/{message.chat.id}.wav')
    n = 2 ** (1.0 * (-4) / 12.0)
    transposed = speed_voice(bowl_sound, n)
    wavfile.write(f'files/{message.chat.id}_.wav', 48000, transposed)


def timbre_up(message):
    fps, bowl_sound = wavfile.read(f'files/{message.chat.id}.wav')
    n = 4
    transposed = pitch_shift(bowl_sound, n)
    wavfile.write(f'files/{message.chat.id}_.wav', 48000, transposed)


def timbre_down(message):
    fps, bowl_sound = wavfile.read(f'files/{message.chat.id}.wav')
    n = -4
    transposed = pitch_shift(bowl_sound, n)
    wavfile.write(f'files/{message.chat.id}_.wav', 48000, transposed)


def pitch_shift(snd_array, n, window_size=2 ** 13, h=2 ** 11):
    """ Changes the pitch of a sound by ``n`` semitones. """
    factor = 2 ** (1.0 * n / 12.0)
    stretched = stretch(snd_array, 1.0 / factor, window_size, h)
    return speed_voice(stretched[window_size:], factor)


def stretch(sound_array, f, window_size, h):
    """ Stretches the sound by a factor `f` """
    phase = np.zeros(window_size)
    hanning_window = np.hanning(window_size)
    result = np.zeros(int(len(sound_array) / f + window_size))

    for i in np.arange(0, len(sound_array) - (window_size + h), h * f):
        # two potentially overlapping subarrays
        a1 = sound_array[int(i): int(i + window_size)]
        a2 = sound_array[int(i + h): int(i + window_size + h)]

        # resynchronize the second array on the first
        s1 = np.fft.fft(hanning_window * a1)
        s2 = np.fft.fft(hanning_window * a2)
        phase = (phase + np.angle(s2 / s1)) % 2 * np.pi
        a2_rephased = np.fft.ifft(np.abs(s2) * np.exp(1j * phase))

        # add to result
        i2 = int(i / f)
        np.add(result[i2: i2 + window_size],
               hanning_window * a2_rephased,
               out=result[i2: i2 + window_size],
               casting="unsafe")

    result = ((2 ** (16 - 4)) * result / result.max())  # normalize (16bit)
    return result.astype('int16')


def speed_voice(sound_array, factor):
    """ Multiplies the sound's speed by some `factor` """
    indices = np.round(np.arange(0, len(sound_array), factor))
    indices = indices[indices < len(sound_array)].astype(int)
    return sound_array[indices.astype(int)]


if __name__ == '__main__':
    thread1 = threading.Thread(target=thread_start)
    thread2 = threading.Thread(target=bot_start)
    thread1.start()
    thread2.start()
