string = {
    "lang-select": "Select a language from the list:\nВыберите язык из списка:",
    "russian": {
        "message": {
            "start": "Главное меню👇🏽",
            "help": "Help",
            "support": "У вас возникли вопросы? Или у вас появилась проблема?\n"
                       "Пиши сюда сообщения, они будут переданы нашим администраторам, и они "
                       "в ближайшее время вам ответят.\nДля выхода из этого раздела напишите мне 'Назад', "
                       "или нажмите на клавиатуре кнопку 'Назад'.",
            "setting": "Выберите из меню какие настройки вы желаете изменить.",
            "pro-account": {
                True: "🥳Pro-аккаунт активирован.🥳 Оплачено дней: ",
                False: "😒*Pro-аккаунт отключен.*😒\n"
                       "Если у вас есть промокод, нажмите на соответствующую кнобку в меню.\n"
                       "Чтобы купить pro-аккаунт, или узнать цену, нажмите на 'Купить pro-аккаунт'.\n"
                       "Если вы оплатили pro-аккаунт, но он не был активирован, нажмите 'Обновить'. "
                       "Если не поможет, напишите нам в службу поддержки.\n"
                       "Если вы хотите получить pro-аккаунт на специальных условиях(на неделю, со скидкой), обратитесь "
                       "к нам в службу поддержки с объяснением, "
                       "почему мы далжны выдать вам pro-аккаунт на специальных условиях.",
                "cancel": "Привет, оплаченое время pro-аккаунта закончилось, теперь вам доступны базовые функции.\n"
                          "Чтобы продолжить пользоваться безлимитом, оплатите pro-аккаунт в разделе Баланс, "
                          "либо в том же разделе введите промокод.",
                "buy-pro": "Купить pro-аккаунт",
                "extend-pro": "Продлить pro-аккаунт",

            },
            "promotion": {
                "none-code": "❌Промокод веден неверно.❌",
                "text": "Отправте мне промокод✏(12 символов) без лишних символов.\n"
                        "Если вы хотите подарить pro-аккаунт другу, можете купить промокод на 1 месяц или на 1 год. "
                        "Для этого нажмите соответствующие кнопки в меню.\n"
                        "Промокод возврату и обмену не подлежит, действителен 30 дней с момента покупки.",
                "by-year": "🎁Вы купили промокод на 1 месяц.\nВаш промокод: ",
                "by-month": "🎁Вы купили промокод на 1 год.\nВаш промокод: ",
                True: "🎉Промокод успешно применен.🎉",
                False: "❌Промокод недействителен.❌\n"
                       "Если вы уверены в действительности промокода, или желаете узнать причину недействительности "
                       "промокода, напишите нам в службу поддержки.",
                "activated": "использовал ваш промокод:",
                "cancel": "Прошел месяц от покупки вами промокода, к великому сожалению он больше не действителен, "
                          "так как его никто не использовал за месяц.\nПромокод:",
            },
            "buy-month": {
                "title": "1 месяц pro-аккаунта",
                "description": "Оплатив 1 месяц pro-аккаунта, у вас разблокируются все возможности, "
                               "не будет приходить реклама, в описании qr-кодов не будет рекламы этого бота, "
                               "а также лишних кнопок целый месяц.",
                "price": "Подписка на 1 месяц",
            },
            "buy-year": {
                "title": "1 год pro-аккаунта",
                "description": "Оплатив 1 год pro-аккаунта, у вас разблокируются все возможности, "
                               "не будет приходить реклама, в описании qr-кодов не будет рекламы этого бота, "
                               "а также лишних кнопок целый год.",
                "price": "Подписка на 1 год",
            },
            "admin-panel": {
                "text": "Вы зашли в панель администратора, выберите из меню что желаете сделать.",
                "all-send-post": "Введите текст, который отправить всем пользователям.",
                "no-pro-send-post": "Введите текст, "
                                    "который отправить всем пользователям, кроме владельцев pro-аккаунта.",
                "one-user-send-post": "Пришлите мне chat-id пользователя, кому отправить пост.",
                "one-user-send-post-text": "Введите текст, который отправить пользователю:\n",
                "statistic": "Статистика пользователей бота:",
                "new-promotion": "Пришлите мне количество дней, на которое необходимо создать промокод.",
                "new-promotion-true": "Промокод:",
            },
            "restart": "🔄Бот успешно перезапущен",
            "advertising": "Изменено в @VoiceEffects\\_bot",
            "answer": {
                "timbre-up": "Секундочку... Повышаю тембр.",
                "timbre-down": "Секундочку... Понижаю тембр.",
                "speed-faster": "Секундочку... Увеличиваю скорость.",
                "speed-slower": "Секундочку... Уменьшаю скорость.",
                "reverb": "Секундочку... Делаю эхо.",
                "delay": "Секундочку... Делаю задержку.",
                "cancel": "Действие отменено.",
                "cannot": "Купите pro-аккаунт.",
                "cannot_voice": "Бесплатные попытки закончились на сегодня.",
                "cannot_timbre": "Бесплатные попытки закончились на сегодня."
            },
            "cancel-result": "Чтобы обрабатывать голосовые сообщения больше 10 секунд, купите pro-аккаунт "
                             "в разделе `Баланс`, либо введите промокод в том же разделе."

        },
        "button": {
            "balance": "💳Баланс",
            "setting": "⚙Настройки",
            "back": "🔙Назад",
            "support": "❗Помощь",
            "help": "ℹСправка",
            "language": "🇷🇺Язык",
            "buy-pro": "💰🔑Купить pro-аккаунт",
            "update": "🔄Обновить",
            "promotion": "🎁Ввести промокод",
            "extend-pro": "💰Продлить pro-аккаунт",
            "buy-month": "📆Купить 1 месяц",
            "buy-year": "📅Купить 1 год",
            "buy-promotion-month": "📆Купить промокод на 1 месяц",
            "buy-promotion-year": "📅Купить промокод на 1 год",
            "admin-panel": "🔐Панель администратора",
            "all-send-post": "Опубликовать пост всем",
            "no-pro-send-post": "Опубликовать пост не pro",
            "one-user-send-post": "Отправить пост одному",
            "statistic": "Получить статистику",
            "new-promotion": "Создать промокод",
            "timbre": "🎼Тембр",
            "speed": "🎧Скорость",
            "background": "🎹Фон",
            "effects": "🔉Эффекты",
            "timbre-up": "Тембр🔼",
            "timbre-down": "🔽Тембр",
            "speed-faster": "Скорость⏭",
            "speed-slower": "⏮Скорость",
            "reverb": "📢Эхо",
            "delay": "🔂Задержка",
            "cancel": "❌Отменить",
        },
        "history": {
            "timbre-up": "✅  Тембр🔼",
            "timbre-down": "✅    🔽Тембр",
            "speed-faster": "✅   Скорость⏭",
            "speed-slower": "✅   ⏮Скорость",
            "reverb": "✅     📢Эхо",
            "delay": "✅  🔂Задержка",
        }
    },
    "english": {
        "message": {
            "start": "Main Menu👇🏽",
            "help": "Help",
            "support": "Do you have any questions? Or do you have a problem?\n"
                       "Write messages here, they will be sent to our administrators, "
                       "and they will respond to you soon.\nTo exit this section, write me 'Back', "
                       "or press the 'Back' button on the keyboard.",
            "setting": "Select from the menu which settings you want to change.",
            "pro-account": {
                True: "🥳Pro account activated.🥳 Paid days: ",
                False: "😒*A Pro-account is disabled.*😒\n"
                       "If you have a promo code, click on the corresponding button in the menu.\n"
                       "To buy a pro account, or find out the price, click on 'Buy a pro-account'.\n"
                       "If you paid for a pro account, but it was not activated, click 'Update'. "
                       "If it doesn't help, please contact our support service.\n"
                       "If you want to get a pro-account on special terms(for a week, with a discount), "
                       "please contact our support service with an explanation "
                       "of why we can give you a pro-account on special terms.",
                "cancel": "Hi, the paid time of the pro account is over, now you have access to basic functions.\n"
                          "To continue using unlimited access, pay for the pro-account in the Balance section, "
                          "or enter the promo code in the same section.",
                "buy-pro": "Buy a pro-account",
                "extend-pro": "Renew your pro-account",

            },
            "promotion": {
                "none-code": "❌The promo code was entered incorrectly.❌",
                "text": "Send me a promo code✏(12 characters) no extra characters.\n"
                        "If you want to give a pro account to a friend, you can buy a promo code for 1 month or 1 year."
                        " To do this, click the appropriate buttons in the menu.\n"
                        "The promo code is non-refundable and non-exchangeable, and is valid for 30 days "
                        "from the date of purchase.",
                "by-year": "🎁You bought a promo code for 1 month.\nYour promo code: ",
                "by-month": "🎁You bought a promo code for 1 year.\nYour promo code: ",
                True: "🎉The promo code was successfully applied.🎉",
                False: "❌The promo code is invalid.❌\n"
                       "If you are sure of the validity of the promo code, or want to know the reason for "
                       "the invalidity of the promo code, write to our support service.",
                "activated": "used your promo code:",
                "cancel": "A month has passed since you bought the promo code, unfortunately it is no longer valid, "
                          "since no one has used it for a month.\nPromo Code:",
            },
            "buy-month": {
                "title": "1 month pro-account",
                "description": "After paying for 1 month of a pro-account, you will unlock all the features, "
                               "no ads will come, there will be no ads for this bot in the description of qr codes, "
                               "as well as extra buttons for a whole month.",
                "price": "1 month subscription",
            },
            "buy-year": {
                "title": "1 year pro-account",
                "description": "Оплатив 1 год pro-аккаунта, у вас разблокируются все возможности, "
                               "не будет приходить реклама, в описании qr-кодов не будет рекламы этого бота, "
                               "а также лишних кнопок целый год.",
                "price": "1 year subscription",
            },
            "admin-panel": {
                "text": "You have entered the admin panel, select from the menu what you want to do.",
                "all-send-post": "Enter the text to send to all users.",
                "no-pro-send-post": "Enter the text that you want to send to all users "
                                    "except the owners of the pro-account.",
                "one-user-send-post": "Send me the chat-id of the user to send the post to.",
                "one-user-send-post-text": "Enter the text to send to the user:\n",
                "statistic": "Bot user statistics:",
                "new-promotion": "Send me the number of days for which you need to create a promo code.",
                "new-promotion-true": "Promo code:",
            },
            "restart": "🔄Bot successfully restarted",
            "advertising": "Changed in @VoiceEffects\\_bot",
            "answer": {
                "timbre-up": "Just a second... Increase the timbre.",
                "timbre-down": "Just a second... Lowering the timbre.",
                "speed-faster": "Just a second... I'm increasing my speed.",
                "speed-slower": "Just a second... I'm slowing down.",
                "reverb": "Just a second... I make an echo.",
                "delay": "Just a second... Making a delay.",
                "cancel": "Action canceled.",
                "cannot": "Buy a pro-account.",
                "cannot_voice": "Free attempts are over for today.",
                "cannot_timbre": "Free attempts are over for today."
            },
            "cancel-result": "To process voice messages for more than 10 seconds, buy a pro account in the "
                             "'Balance' section, or enter a promo code in the same section."

        },
        "button": {
            "balance": "💳Balance",
            "setting": "⚙Settings",
            "back": "🔙Back",
            "support": "❗Support",
            "help": "ℹHelp",
            "language": "🇺🇸Language",
            "buy-pro": "💰🔑Buy pro-account",
            "update": "🔄Update",
            "promotion": "🎁Enter a promo code",
            "extend-pro": "💰Extended pro-account",
            "buy-month": "📆Buy 1 month",
            "buy-year": "📅Buy 1 year",
            "buy-promotion-month": "📆Buy a promo code for 1 month",
            "buy-promotion-year": "📅Buy a promo code for 1 year",
            "admin-panel": "🔐Admin Panel",
            "all-send-post": "Опубликовать пост всем",
            "no-pro-send-post": "Опубликовать пост не pro",
            "one-user-send-post": "Отправить пост одному",
            "statistic": "Получить статистику",
            "new-promotion": "Создать промокод",
            "timbre": "🎼Timbre",
            "speed": "🎧Speed",
            "background": "🎹Background",
            "effects": "🔉Effects",
            "timbre-up": "Timbre🔼",
            "timbre-down": "🔽Timbre",
            "speed-faster": "Speed⏭",
            "speed-slower": "⏮Speed",
            "reverb": "📢Echo",
            "delay": "🔂Delay",
            "cancel": "❌Cancel",
        },
        "history": {
            "timbre-up": "✅  Timbre🔼",
            "timbre-down": "✅    🔽Timbre",
            "speed-faster": "✅   Speed⏭",
            "speed-slower": "✅   ⏮Speed",
            "reverb": "✅     📢Reverb",
            "delay": "✅  🔂Delay",
        }
    }
}
